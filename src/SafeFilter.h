//
// Created by simon on 03.06.20.
//

#ifndef DECIMALSEPARATOR_SAFEFILTER_H
#define DECIMALSEPARATOR_SAFEFILTER_H

#include "mlt++/MltFilter.h"
#include "mlt++/MltProfile.h"

class SafeFilter : Mlt::Filter {

public:
    SafeFilter(Mlt::Profile &profile, const char *id, const char *service = nullptr);
    ~SafeFilter() override;

    char *getProperty(const char *name);

    Mlt::Filter *m_mltFilter;
};


#endif //DECIMALSEPARATOR_SAFEFILTER_H
