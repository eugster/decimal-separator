//
// Created by simon on 04.06.20.
//

#include "SafePlaylist.h"

SafePlaylist::SafePlaylist(Mlt::Profile &profile) {
    mltPlaylist = new Mlt::Playlist(profile);
}

SafePlaylist::~SafePlaylist() {
    delete mltPlaylist;
}

int SafePlaylist::attachFilter(SafeFilter &filter) {
    return mltPlaylist->attach(*filter.m_mltFilter);
}

