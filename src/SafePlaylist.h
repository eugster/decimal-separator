//
// Created by simon on 04.06.20.
//

#ifndef DECIMALSEPARATOR_SAFEPLAYLIST_H
#define DECIMALSEPARATOR_SAFEPLAYLIST_H

#include "mlt++/MltProfile.h"
#include "mlt++/MltPlaylist.h"
#include "SafeFilter.h"

class SafePlaylist {
public:
    SafePlaylist( Mlt::Profile& profile );
    ~SafePlaylist();

    int attachFilter( SafeFilter &filter );

    Mlt::Playlist *mltPlaylist;
};


#endif //DECIMALSEPARATOR_SAFEPLAYLIST_H
