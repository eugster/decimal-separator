//
// Created by simon on 03.06.20.
//

#include <string.h>
#include "SafeFilter.h"

SafeFilter::SafeFilter(Mlt::Profile &profile, const char *id, const char *service) : Filter(profile, id, service) {
    m_mltFilter = new Mlt::Filter(profile, id, service);
}

SafeFilter::~SafeFilter() {
    delete m_mltFilter;
}

char *SafeFilter::getProperty(const char *name) {
    auto currentLocale = strdup(setlocale(LC_ALL, nullptr));
    setlocale(LC_ALL, "C");
    auto result = m_mltFilter->get(name);
    setlocale(LC_ALL, currentLocale);
    return result;
}
