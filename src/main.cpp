#include <clocale>
#include <memory>
#include <QtGlobal>
#include <QtCore/QString>
#include <QtCore/QLocale>
#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QStandardPaths>

#include "framework/mlt.h"
#include "mlt++/MltConsumer.h"
#include "mlt++/MltFactory.h"
#include "mlt++/MltFilter.h"
#include "mlt++/MltProfile.h"
#include "mlt++/MltPlaylist.h"
#include "mlt++/MltProducer.h"
#include "mlt++/MltRepository.h"
#include "SafeFilter.h"
#include "SafePlaylist.h"

void forceCLocale(){
    std::setlocale(LC_ALL, "C");
    ::qputenv("LC_ALL", "C");
}

void printLocaleInfo(const QString &description, const QLocale &locale) {
    double converted;
    bool success;
    qInfo() << description << locale.name();
    qInfo() << "Decimal point is " << locale.decimalPoint();
    qInfo() << "Number to string in this locale: 23456789.42042042 → " << locale.toString(23456789.42042042, 'f');
    qInfo() << "String to number in this locale with ,: " << locale.toDouble("23456789,42042042");
    qInfo() << "String to number in this locale with .: " << locale.toDouble("23456789.42042042");
    converted = QString("23456789,42042042").toDouble(&success);
    qInfo() << "String to double with QString with ,:" << (success ? "successful" : "failed") << converted;
    converted = QString("23456789.42042042").toDouble(&success);
    qInfo() << "String to double with QString with .:" << (success ? "successful" : "failed") << converted;
    converted = QString("12 345").toDouble(&success);
    qInfo() << "String with space to double with QString with .:" << (success ? "successful" : "failed") << converted;
    qInfo() << "Double to string with QString::number: " << QString::number(23456789.42042042);
    qInfo() << "Double to string with QString::number( , 'f', 10): " << QString::number(23456789.42042042, 'f', 10);
}

void printMltInfo(const QLocale &locale, short verbose) {
    std::unique_ptr<Mlt::Repository> m_repository = std::unique_ptr<Mlt::Repository>(Mlt::Factory::init());

    // Retrieve the list of available producers.
    QScopedPointer<Mlt::Properties> producers(m_repository->producers());
    QStringList producersList;
    int nb_producers = producers->count();
    producersList.reserve(nb_producers);
    for (int i = 0; i < nb_producers; ++i) {
        producersList << producers->get_name(i);
    }
    qDebug() << "Producers:" << producersList;
    mlt_log_set_level(MLT_LOG_WARNING);
    //mlt_log_set_callback(mlt_log_handler);

    if (verbose > 2) {
        QScopedPointer<Mlt::Properties> assets(m_repository->filters());
        int max = assets->count();
        for (int i = 0; i < max; ++i) {
            qDebug() << assets->get_name(i);
        }
    }

    bool ok = false;
    QScopedPointer<Mlt::Properties> metadata(
            m_repository->metadata(filter_type, QString("frei0r.defish0r").toLatin1().data()));
    if (metadata && metadata->is_valid()) {
        if (metadata->get("title") && metadata->get("identifier") && strlen(metadata->get("title")) > 0) {
            ok = true;
            QString id = metadata->get("identifier");
            qDebug() << "Identifier: " << id;

            if (verbose > 0) {
                Mlt::Properties param_props((mlt_properties) metadata->get_data("parameters"));
                for (int j = 0; param_props.is_valid() && j < param_props.count(); ++j) {

                    Mlt::Properties paramdesc((mlt_properties) param_props.get_data(param_props.get_name(j)));
                    qDebug() << "ID: " << paramdesc.get("identifier")
                             << "Type: " << paramdesc.get("type")
                             << "Title: " << paramdesc.get("title")
                             << "\n\t" << paramdesc.get("description");
                }
            }
        }
    }
    if (!ok) qDebug() << "Could not load metadata";

    auto profile = new Mlt::Profile();
    auto playlist = new Mlt::Playlist(*profile);
    auto filter = new Mlt::Filter(*profile, "frei0r.defish0r");

    filter->set_lcnumeric("C");

    // Amount is a float, but MLT allows to set it to a string. We do not do this YET …
    auto result = filter->set("Amount", 42.4242);
    // but NOW with filter->get() which internally converts the number property to a string property (with the current locale)
    // and then returns it. The property will remain a string property forever now.
    qDebug() << "Set filter prop Amount to " << filter->get("Amount") << " (return code " << result << ")";
    if (verbose > 0) {
        Mlt::Properties props(filter->get_properties());
        for (int i = 0; i < props.count(); i++) {
            qDebug() << "Filter Property " << i << ": " << props.get_name(i);
        }
    }

    auto vignette = new Mlt::Filter(*profile, "frei0r.vignette");
    vignette->set_lcnumeric("hu_HU.utf8");
    vignette->set("aspect", 21.2121);


    QString serialisedPlaylist;
    QString root("/tmp/xml-document-root");
    Mlt::Consumer xmlConsumer(*profile, "xml", "fullPath");
    if (!root.isEmpty()) {
        xmlConsumer.set("root", root.toUtf8().constData());
    }

    Mlt::Producer producer(*profile, "color:blue");
    playlist->append(producer);
    playlist->attach(*filter);
    playlist->attach(*vignette);

    assert(xmlConsumer.is_valid());

    qDebug() << "Before running XML consumer, setting locale to " << locale.name().toStdString().c_str();
    // Try plain variant (e.g. for C) and .utf8 variant (e.g. for de_CH.utf8)
    auto oldLocale = strdup(std::setlocale(LC_ALL, nullptr));
    qDebug() << "LC_ALL was " << oldLocale;
    std::setlocale(LC_ALL, locale.name().toStdString().c_str());
    std::setlocale(LC_ALL, (locale.name().toStdString() + ".utf8").c_str());
    xmlConsumer.set("store", "kdenlive");
    xmlConsumer.set("time_format", "clock");
    xmlConsumer.connect(*playlist);
    xmlConsumer.run();
    serialisedPlaylist = QString::fromUtf8(xmlConsumer.get("fullPath"));
    qDebug() << serialisedPlaylist;
    std::setlocale(LC_ALL, oldLocale);
}

void printMltInfoSafe(const QLocale &locale) {
    QLocale::setDefault(locale);
    std::setlocale(LC_ALL, locale.name().toStdString().c_str());
    std::setlocale(LC_ALL, (locale.name().toStdString() + ".utf8").c_str());

    mlt_log_set_level(MLT_LOG_WARNING);

    auto profile = new Mlt::Profile();
    auto playlist = new SafePlaylist(*profile);
    auto filter = new SafeFilter(*profile, "frei0r.defish0r");

    auto result = filter->m_mltFilter->set("Amount", 42.4242);
    qDebug() << "Set filter prop Amount to " << filter->getProperty("Amount") << " (return code " << result << ")";

    auto vignette = new SafeFilter(*profile, "frei0r.vignette");
    vignette->m_mltFilter->set("aspect", 21.2121);
    vignette->m_mltFilter->set("unicode", "«Hällou» – ½ Ԙ");


    QString serialisedPlaylist;
    QString root("/tmp/xml-document-root");
    Mlt::Consumer xmlConsumer(*profile, "xml", "fullPath");
    if (!root.isEmpty()) {
        xmlConsumer.set("root", root.toUtf8().constData());
    }

    Mlt::Producer producer(*profile, "color:blue");
    playlist->mltPlaylist->append(producer);
    playlist->attachFilter(*filter);
    playlist->attachFilter(*vignette);

    auto prevLocale = strdup(std::setlocale(LC_ALL, nullptr));
    std::setlocale(LC_ALL, "C");
    assert(xmlConsumer.is_valid());
    xmlConsumer.set("store", "kdenlive");
    xmlConsumer.set("time_format", "clock");
    xmlConsumer.connect(*playlist->mltPlaylist);
    xmlConsumer.run();
    serialisedPlaylist = QString::fromUtf8(xmlConsumer.get("fullPath"));
    std::setlocale(LC_ALL, prevLocale);

    qDebug() << serialisedPlaylist;
}


void readMlt() {
    QFile inFile("/data/cworkspace/decimal-separator/samples/test.xml");
    inFile.open(QFile::ReadOnly);
    auto profile = new Mlt::Profile();
    auto xmlData = QString(inFile.readAll());
    qDebug() << "XML data: " << xmlData;
    QScopedPointer<Mlt::Producer> xmlProd(new Mlt::Producer(*profile, "xml-string", xmlData.toStdString().c_str()));

    Mlt::Properties props(xmlProd->get_properties());
    for (int i = 0; i < props.count(); i++) {
        qDebug() << "XML Property " << i << ": " << props.get_name(i) << "=" << props.get(i);
    }
}


int main() {
    QLocale defaultLocale;
    QLocale hungarianLocale(QLocale::Hungarian);
    QLocale swissLocale(QLocale::SwissGerman);
    QLocale cLocale = QLocale::c();

    printLocaleInfo("Default locale", QLocale());
    printLocaleInfo("Hungarian locale", hungarianLocale);
    printLocaleInfo("Swiss German locale", swissLocale);
    printLocaleInfo("C locale", cLocale);


    qInfo() << "Setting the default locale to C";
    QLocale::setDefault(QLocale::c());
    printLocaleInfo("Default locale", QLocale());

    printMltInfo(cLocale, 1);
    printMltInfo(hungarianLocale, false);
    printMltInfo(swissLocale, false);

    qDebug() << "Safe version";
    printMltInfoSafe(cLocale);
    printMltInfoSafe(hungarianLocale);
    printMltInfoSafe(swissLocale);

    readMlt();
}