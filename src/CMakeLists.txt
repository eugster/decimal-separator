### Find packages files
find_package(LibV4L2)
set_package_properties(LibV4L2 PROPERTIES
    DESCRIPTION "video4linux support libraries"
    URL "https://linuxtv.org/downloads/v4l-utils/"
    TYPE RUNTIME
    PURPOSE "Required for better webcam support")

if(NOT WIN32)
    find_package(PkgConfig QUIET)
    execute_process(
        COMMAND ${PKG_CONFIG_EXECUTABLE} --variable=mltdatadir mlt-framework
        OUTPUT_VARIABLE MLT_DATADIR
        RESULT_VARIABLE MLT_DATADIR_failed)
    if(NOT MLT_DATADIR_failed)
        string(REGEX REPLACE "[\r\n]" "" MLT_DATADIR "${MLT_DATADIR}")
    endif()

    execute_process(
        COMMAND ${PKG_CONFIG_EXECUTABLE} --variable=meltbin mlt-framework
        OUTPUT_VARIABLE MLT_MELTBIN
        RESULT_VARIABLE MLT_MELTBIN_failed)
    if(NOT MLT_MELTBIN_failed)
        string(REGEX REPLACE "[\r\n]" "" MLT_MELTBIN "${MLT_MELTBIN}")
    endif()
else()
    set(MLT_MELTBIN "melt.exe")
    set(MLT_DATADIR "../share/mlt")
endif()
configure_file(mlt_config.h.in ${CMAKE_BINARY_DIR}/mlt_config.h)


file(GLOB top_SRCS "*.cpp")
# string(REGEX REPLACE "${CMAKE_CURRENT_SOURCE_DIR}/lib/[^;]+;?" "" top_SRCS "${top_SRCS}")
list(REMOVE_ITEM top_SRCS ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp)

add_executable(DecimalSeparator main.cpp SafeFilter.cpp SafeFilter.h SafePlaylist.cpp SafePlaylist.h)
target_include_directories(DecimalSeparator
    PRIVATE ${CMAKE_BINARY_DIR}
    )
target_include_directories(DecimalSeparator
	SYSTEM PRIVATE ${MLT_INCLUDE_DIR} ${MLTPP_INCLUDE_DIR})
target_include_directories(DecimalSeparator
	SYSTEM  PRIVATE ${MLT_INCLUDE_DIR} ${MLTPP_INCLUDE_DIR})

target_link_libraries(DecimalSeparator
    Qt5::Svg
    Qt5::Quick
    Qt5::QuickControls2
    Qt5::QuickWidgets
    Qt5::Concurrent
    Qt5::Multimedia
    ${OPENGL_LIBRARIES}
    ${OPENGLES_LIBRARIES}
    ${MLT_LIBRARIES}
    ${MLTPP_LIBRARIES}
    ${CMAKE_DL_LIBS}
    ${CMAKE_THREAD_LIBS_INIT}
)
if(TARGET RTTR::Core)
     target_link_libraries(DecimalSeparator RTTR::Core)
else()
     target_link_libraries(DecimalSeparator RTTR::Core_Lib)
endif()

if(BUILD_COVERAGE)
  target_link_libraries(DecimalSeparator gcov)
endif()
set_property(TARGET DecimalSeparator PROPERTY CXX_STANDARD 14)
SET(KDENLIVE_CXX_FLAGS  "${DEFAULT_CXX_FLAGS} -Wall -pedantic -Wextra")
SET(KDENLIVE_CXX_FLAGS "${KDENLIVE_CXX_FLAGS} -Wcast-qual -Wcast-align -Wfloat-equal -Wpointer-arith")
SET(KDENLIVE_CXX_FLAGS "${KDENLIVE_CXX_FLAGS} -Wunreachable-code -Wchar-subscripts -Wcomment -Wformat")
SET(KDENLIVE_CXX_FLAGS "${KDENLIVE_CXX_FLAGS} -Wmain -Wmissing-braces")
SET(KDENLIVE_CXX_FLAGS "${KDENLIVE_CXX_FLAGS} -Wparentheses -Wsequence-point -Wreturn-type -Wswitch")
SET(KDENLIVE_CXX_FLAGS "${KDENLIVE_CXX_FLAGS} -Wuninitialized -Wreorder -Wundef -Wshadow -Wwrite-strings")
SET(KDENLIVE_CXX_FLAGS "${KDENLIVE_CXX_FLAGS} -Wmissing-noreturn -Wsign-compare -Wsign-conversion -Wunused")
SET(KDENLIVE_CXX_FLAGS "${KDENLIVE_CXX_FLAGS} -Wstrict-aliasing -Wconversion -Wdisabled-optimization -Wno-undef")
SET(KDENLIVE_CXX_FLAGS "${KDENLIVE_CXX_FLAGS} -Wunused-parameter -Wshadow -Wno-variadic-macros -Wno-float-conversion")
if(KDENLIVE_COMPILER_IS_GNUCXX)
    SET(KDENLIVE_CXX_FLAGS "${KDENLIVE_CXX_FLAGS} -Wlogical-op -Wunsafe-loop-optimizations ")
endif()
set_target_properties(DecimalSeparator PROPERTIES COMPILE_FLAGS "${KDENLIVE_CXX_FLAGS}")
add_definitions(${qt5gui_definitions} -DQT_STRICT_ITERATORS -DQT_NO_CAST_FROM_BYTEARRAY)
# To compile kiss_fft.
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} --std=c99")

# Optional deps
if(DRMINGW_FOUND)
    target_compile_definitions(DecimalSeparator PRIVATE -DUSE_DRMINGW)
    target_include_directories(DecimalSeparator SYSTEM PRIVATE ${DRMINGW_INCLUDE_DIR})
    target_link_libraries(DecimalSeparator ${DRMINGW_LIBRARY})
elseif(KF5Crash_FOUND)
    target_compile_definitions(DecimalSeparator PRIVATE -DKF5_USE_CRASH)
    target_link_libraries(DecimalSeparator KF5::Crash)
endif()

if(KF5_PURPOSE)
    target_compile_definitions(DecimalSeparator PRIVATE -DKF5_USE_PURPOSE)
    target_link_libraries(DecimalSeparator KF5::Purpose KF5::PurposeWidgets)
endif()

if(Q_WS_X11)
	target_include_directories(DecimalSeparator SYSTEM PRIVATE ${X11_Xlib_INCLUDE_PATH})
    target_link_libraries(DecimalSeparator ${X11_LIBRARIES})
endif()



if(LIBV4L2_FOUND)
    target_include_directories(DecimalSeparator SYSTEM PRIVATE ${LIBV4L2_INCLUDE_DIR})
    target_link_libraries(DecimalSeparator ${LIBV4L2_LIBRARY})
    target_compile_definitions(DecimalSeparator PRIVATE -DUSE_V4L)
endif()
